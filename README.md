# A CTypes Dll example #

Just a quick sample of a dll that sums `double` values in C++, interfaced through Python. Compile the C++ side on Linux with `make` inside the `summer` directory.

The Python wrapper tries to pass zero-copy the numpy arrays of the correct shape/dtype, and creates a temporary copy for the rest.
